package com.abc.student.exception;

public class StudentListNotFoundException extends Exception {

	private static final long serialVersionUID = 1L;

	public StudentListNotFoundException(String message) {
		super(message);
	}

}
