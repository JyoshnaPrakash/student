package com.abc.student.exception;

public class InvalidStudentIdException extends Exception {

	private static final long serialVersionUID = 1L;

	public InvalidStudentIdException(String message) {
		super(message);
	}

}
