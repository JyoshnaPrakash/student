package com.abc.student.service;

import java.util.List;
import java.util.Optional;

import com.abc.student.dto.StudentRegistrationRequestDto;
import com.abc.student.dto.StudentResponseDto;
import com.abc.student.entity.Student;
import com.abc.student.exception.InvalidStudentIdException;
import com.abc.student.exception.StudentRegistrationException;

public interface StudentService {

	StudentResponseDto register(StudentRegistrationRequestDto studentRequestDto) throws StudentRegistrationException;

	public StudentRegistrationRequestDto getStudent(Integer studentId) throws InvalidStudentIdException;
	
	public Optional<List<Student>> getStudents();
	
	public StudentResponseDto changeStudentDetails(StudentRegistrationRequestDto studentRegistrationRequestDto, Integer studentId)
			throws InvalidStudentIdException;
	
	public StudentResponseDto deleteStudent(Integer studentId) throws InvalidStudentIdException;
}
