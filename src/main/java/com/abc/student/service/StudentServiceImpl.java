package com.abc.student.service;

import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.abc.student.dto.StudentRegistrationRequestDto;
import com.abc.student.dto.StudentResponseDto;
import com.abc.student.entity.Student;
import com.abc.student.exception.InvalidStudentIdException;
import com.abc.student.exception.StudentRegistrationException;
import com.abc.student.repository.StudentRepository;
import com.abc.student.utils.StudentConstants;

@Service
public class StudentServiceImpl implements StudentService {

	@Autowired
	StudentRepository studentRepository;

	@Override
	public StudentResponseDto register(StudentRegistrationRequestDto studentRequestDto)
			throws StudentRegistrationException {

		if (!validateFirstName(studentRequestDto.getFirstName())) {
			throw new StudentRegistrationException(StudentConstants.INVALID_USER_NAME);
		}

		if (!validateLastName(studentRequestDto.getLastName())) {
			throw new StudentRegistrationException(StudentConstants.INVALID_USER_NAME);
		}

		if (!validPhoneNumber(studentRequestDto.getPhoneNumber())) {
			throw new StudentRegistrationException(StudentConstants.INVALID_MOBILE_NUMBER);
		}

		if (!validEmailId(studentRequestDto.getEmailId())) {
			throw new StudentRegistrationException(StudentConstants.INVALID_EMAIL);
		}
		if (!validRollNo(studentRequestDto.getRollNo())) {
			throw new StudentRegistrationException(StudentConstants.INVALID_ROll_NO);
		}

		Student checkCustomerEmail = studentRepository.findByEmailId(studentRequestDto.getEmailId());
		if (checkCustomerEmail != null) {
			throw new StudentRegistrationException(StudentConstants.EXIST_EMAIL);
		}

		Student checkMobile = studentRepository.findByPhoneNumber(studentRequestDto.getPhoneNumber());
		if (checkMobile != null) {
			throw new StudentRegistrationException(StudentConstants.EXIST_MOBILE_NUMBER);
		}

		Student checkCustomerPanNo = studentRepository.findByRollNo(studentRequestDto.getRollNo());
		if (checkCustomerPanNo != null) {
			throw new StudentRegistrationException(StudentConstants.EXIST_ROLL_NO);
		}

		Student student = new Student();
		student.setFirstName(studentRequestDto.getFirstName());
		student.setLastName(studentRequestDto.getLastName());
		student.setRollNo(studentRequestDto.getRollNo());
		student.setEmailId(studentRequestDto.getEmailId());
		student.setPhoneNumber(studentRequestDto.getPhoneNumber());
		student.setMathematicsScore(studentRequestDto.getMathematicsScore());
		student.setScienceScore(studentRequestDto.getScienceScore());
		student.setSocialScore(studentRequestDto.getSocialScore());
		student.setEnglishScore(studentRequestDto.getEnglishScore());
		student.setHindiScore(studentRequestDto.getHindiScore());
		Student studentRepsonse = studentRepository.save(student);

		StudentResponseDto studentResponseDto = new StudentResponseDto();
		studentResponseDto.setStudentId(studentRepsonse.getStudentId());
		studentResponseDto.setStatusCode(201);
		studentResponseDto.setMessage("Student Registered successfully, above is your track Id");
		return studentResponseDto;
	}

	private boolean validateFirstName(String firstName) {
		String name = ("^[a-zA-Z]*$");
		return firstName.matches(name);
	}

	private boolean validateLastName(String lastName) {
		String name = ("^[a-zA-Z]*$");
		return lastName.matches(name);
	}

	private boolean validEmailId(String email) {
		Pattern p = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
		java.util.regex.Matcher m = p.matcher(email);
		return (m.find() && m.group().equals(email));
	}

	private boolean validPhoneNumber(Long number) {
		String num = number.toString();
		Pattern p = Pattern.compile("^[0-9]{10}$");
		java.util.regex.Matcher m = p.matcher(num);
		return (m.find() && m.group().equals(num));
	}

	private boolean validRollNo(String panNo) {
		Pattern p = Pattern.compile("^[a-zA-Z1-9]{10}$", Pattern.CASE_INSENSITIVE);
		java.util.regex.Matcher m = p.matcher(panNo);
		return (m.find() && m.group().equals(panNo));
	}

	@Override
	public StudentRegistrationRequestDto getStudent(Integer studentId) throws InvalidStudentIdException {
		Optional<Student> student = studentRepository.findById(studentId);
		if (student.isPresent()) {
			StudentRegistrationRequestDto studentRegistrationRequestDto = new StudentRegistrationRequestDto();
			studentRegistrationRequestDto.setFirstName(student.get().getFirstName());
			studentRegistrationRequestDto.setLastName(student.get().getLastName());
			studentRegistrationRequestDto.setRollNo(student.get().getRollNo());
			studentRegistrationRequestDto.setEmailId(student.get().getEmailId());
			studentRegistrationRequestDto.setPhoneNumber(student.get().getPhoneNumber());
			studentRegistrationRequestDto.setMathematicsScore(student.get().getMathematicsScore());
			studentRegistrationRequestDto.setScienceScore(student.get().getScienceScore());
			studentRegistrationRequestDto.setSocialScore(student.get().getSocialScore());
			studentRegistrationRequestDto.setEnglishScore(student.get().getEnglishScore());
			studentRegistrationRequestDto.setHindiScore(student.get().getHindiScore());

			return studentRegistrationRequestDto;
		} else {
			throw new InvalidStudentIdException(StudentConstants.STUDENT_INFO_NOT_FOUND);
		}
	}

	@Override
	public Optional<List<Student>> getStudents() {
		List<Student> hospitalList = studentRepository.findAll();
		return Optional.of(hospitalList);
	}

	@Override
	public StudentResponseDto changeStudentDetails(StudentRegistrationRequestDto studentRegistrationRequestDto,
			Integer studentId) throws InvalidStudentIdException {
		StudentResponseDto studentResponseDto = new StudentResponseDto();
		Optional<Student> studentRecord = Optional.ofNullable(studentRepository.findByStudentId(studentId));
		if (studentRecord.isPresent()) {
			Student student = studentRecord.get();
			student.setFirstName(studentRegistrationRequestDto.getFirstName());
			student.setLastName(studentRegistrationRequestDto.getLastName());
			student.setRollNo(studentRegistrationRequestDto.getRollNo());
			student.setEmailId(studentRegistrationRequestDto.getEmailId());
			student.setPhoneNumber(studentRegistrationRequestDto.getPhoneNumber());
			student.setMathematicsScore(studentRegistrationRequestDto.getMathematicsScore());
			student.setScienceScore(studentRegistrationRequestDto.getScienceScore());
			student.setSocialScore(studentRegistrationRequestDto.getSocialScore());
			student.setEnglishScore(studentRegistrationRequestDto.getEnglishScore());
			student.setHindiScore(studentRegistrationRequestDto.getHindiScore());
			studentRepository.save(student);
		} else {
			throw new InvalidStudentIdException(StudentConstants.STUDENT_INFO_NOT_FOUND);
		}
		studentResponseDto.setMessage("Success");
		studentResponseDto.setStatusCode(200);

		return studentResponseDto;
	}

	@Override
	public StudentResponseDto deleteStudent(Integer studentId) throws InvalidStudentIdException {
		StudentResponseDto studentResponseDto = new StudentResponseDto();
		Optional<Student> student = Optional.ofNullable(studentRepository.findByStudentId(studentId));
		if (student.isPresent()) {
			studentRepository.deleteById(studentId);
		} else {
			throw new InvalidStudentIdException(StudentConstants.STUDENT_INFO_NOT_FOUND);
		}
		studentResponseDto.setMessage("Success");
		studentResponseDto.setStatusCode(200);
		return studentResponseDto;
	}

}
