package com.abc.student.dto;

public class StudentRegistrationRequestDto {

	private String firstName;
	private String lastName;
	private String rollNo;
	private String emailId;
	private Long phoneNumber;
	private Integer mathematicsScore;
	private Integer scienceScore;
	private Integer socialScore;
	private Integer englishScore;
	private Integer hindiScore;

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getRollNo() {
		return rollNo;
	}

	public void setRollNo(String rollNo) {
		this.rollNo = rollNo;
	}

	public Long getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(Long phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Integer getMathematicsScore() {
		return mathematicsScore;
	}

	public void setMathematicsScore(Integer mathematicsScore) {
		this.mathematicsScore = mathematicsScore;
	}

	public Integer getScienceScore() {
		return scienceScore;
	}

	public void setScienceScore(Integer scienceScore) {
		this.scienceScore = scienceScore;
	}

	public Integer getSocialScore() {
		return socialScore;
	}

	public void setSocialScore(Integer socialScore) {
		this.socialScore = socialScore;
	}

	public Integer getEnglishScore() {
		return englishScore;
	}

	public void setEnglishScore(Integer englishScore) {
		this.englishScore = englishScore;
	}

	public Integer getHindiScore() {
		return hindiScore;
	}

	public void setHindiScore(Integer hindiScore) {
		this.hindiScore = hindiScore;
	}

}
