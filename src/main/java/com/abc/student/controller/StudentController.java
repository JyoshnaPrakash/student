package com.abc.student.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.abc.student.dto.StudentRegistrationRequestDto;
import com.abc.student.dto.StudentResponseDto;
import com.abc.student.entity.Student;
import com.abc.student.exception.InvalidStudentIdException;
import com.abc.student.exception.StudentListNotFoundException;
import com.abc.student.exception.StudentRegistrationException;
import com.abc.student.service.StudentService;
import com.abc.student.utils.StudentConstants;

@RestController
@RequestMapping("/")
public class StudentController {

	@Autowired
	StudentService studentService;

	@PostMapping("/new")
	public ResponseEntity<StudentResponseDto> register(
			@RequestBody StudentRegistrationRequestDto studentRegistrationRequestDto)
			throws StudentRegistrationException {
		StudentResponseDto studentResponseDto = studentService.register(studentRegistrationRequestDto);
		return new ResponseEntity<>(studentResponseDto, HttpStatus.CREATED);
	}

	@GetMapping("/{studentId}/get")
	public ResponseEntity<StudentRegistrationRequestDto> getStudentDetails(@RequestParam("studentId") Integer studentId)
			throws InvalidStudentIdException {

		StudentRegistrationRequestDto studentRegistrationRequestDto = studentService.getStudent(studentId);
		return new ResponseEntity<>(studentRegistrationRequestDto, HttpStatus.OK);

	}

	@GetMapping("/")
	public ResponseEntity<List<Student>> getStudentList() throws StudentListNotFoundException {
		Optional<List<Student>> studentOptional = studentService.getStudents();
		List<Student> studentList = new ArrayList<>();
		if (studentOptional.isPresent()) {
			studentList = studentOptional.get();
			if (studentList.isEmpty()) {
				throw new StudentListNotFoundException(StudentConstants.STUDENT_INFO_NOT_FOUND);
			}
		}
		return new ResponseEntity<>(studentList, HttpStatus.OK);

	}

	@PutMapping("/change/{studentId}")
	public ResponseEntity<StudentResponseDto> changeStudentDetails(

			@RequestBody StudentRegistrationRequestDto studentRegistrationRequestDto, @RequestParam Integer studentId)
			throws InvalidStudentIdException {
		return new ResponseEntity<>(studentService.changeStudentDetails(studentRegistrationRequestDto, studentId),
				HttpStatus.OK);
	}

	@DeleteMapping("/student/delete/{studentId}")
	public ResponseEntity<StudentResponseDto> deleteStudent(@PathVariable(value = "studentId") Integer studentId)
			throws InvalidStudentIdException {
		StudentResponseDto studentResponseDto = studentService.deleteStudent(studentId);
		return new ResponseEntity<>(studentResponseDto, HttpStatus.OK);

	}

}
