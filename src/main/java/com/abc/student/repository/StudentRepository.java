package com.abc.student.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.abc.student.entity.Student;

public interface StudentRepository extends JpaRepository<Student, Integer> {

	Student findByStudentId(Integer studentId);

	Student findByEmailId(String emailId);

	Student findByPhoneNumber(Long phoneNumber);

	Student findByRollNo(String rollNo);

}
