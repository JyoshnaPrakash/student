package com.abc.student.utils;

public class StudentConstants {

	private StudentConstants() {
		throw new IllegalStateException("Utility class");
	}

	public static final String INVALID_USER_NAME = "Please enter name in string";
	public static final String INVALID_MOBILE_NUMBER = "Please enter a valid mobile number";
	public static final String INVALID_EMAIL = "please enter a valid email Id";
	public static final String EXIST_EMAIL = "Email Id already exist";
	public static final String INVALID_ROll_NO = "Invalid roll number";
	public static final String REGISTRATION_FAILED = "Registration failed..";

	public static final String EXIST_ROLL_NO = "Roll Number already exists";
	public static final String EXIST_MOBILE_NUMBER = "Select a new mobile number";

	public static final String STUDENT_INFO_NOT_FOUND = "No student exists";
}
